#!/bin/bash

read word

curl "http://googledictionary.freecollocation.com/meaning?word=$word" 2> /dev/null > general

echo -n "Transcription: "
cat general | grep "photo.jpg" | cut -d "/" -f2
echo -n "Description: "
cat general | grep -A10 "show_ads" | grep -A1 "list-style:decimal" | head -n 2 | tail -n 1
echo -n "Example: "
#cat general | grep -A2 "color:#767676;list-style:none" | head -n 3 | tail -n 1 | sed -e "s/<.*>/$word/g"
cat general | grep -A2 "color:#767676;list-style:none" | head -n 3 | tail -n 1 | sed -e 's/<[^>]*>//g'



curl "https://www.thesaurus.com/browse/$word " 2> /dev/null> general; 
sed -i -e 's/133coio/\n/g' general;

syno_selection=`cat general | grep -n "Synonyms" | tail -n 1 | cut -d ':' -f1` 
anto_selection=`cat general | grep -n "Antonyms" | tail -n 1 | cut -d ':' -f1` 

cat general | head -n  $anto_selection > syno
cat general | tail -n +$anto_selection > anto

echo -n "Synonyms: "
cat syno | grep "browse/" | cut -d '/' -f3 | cut -d '"' -f1 | tail -n +4 | head -n 1

echo -n "Antonyms: "
cat anto | grep "browse/" | cut -d '/' -f3 | cut -d '"' -f1 | tail -n +2 | head -n 1
